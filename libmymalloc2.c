/*	
*	Programmed by Michael Humphrey
*	libmymalloc2 is a paralized version of mymalloc and free. The purpose of the program is
*	to simulate the malloc and free functions provided in the c library. The program intializes
*	256k bytes * 8 to allocate to the user upon request. The actual amount of memory reserved upon
*	startup will be slightly larger since it will include metadata needed to maintain lists.
*	Two lists will be available for allocation from a small list, that will include blocks less than
*	or equal to 64 bytes and a large list that will handle anything larger than 64 bytes.
*	The larger list will hand out 1k for all requests and the small list will allocate
*	64 bytes. The lists will be maintained as a singly linked list that will be accessed
*	like a stack.
*/

#include "myMalloc.h"
#include "libmymalloc2.h"


struct LINKED_LIST_SMALL *headSmallList; // pointer to the start of the list of structs, can contain either large or small blocks
struct LINKED_LIST_LARGE *headLargeList; // pointer to the start of the list of structs, can contain either large or small blocks

struct LARGE_BLOCK *largeStack; // used to point to the current free memory in the large list
struct SMALL_BLOCK *smallStack; // used to point to the current free memory in the small list

int numSmallBlocks = 0; // keep count of the number of small blocks, since they will be maintained in one single stack
int numLargeBlocks = 0; // keep count of the number of large blocks, since they will be maintained in one single stack

pthread_mutex_t L;

/*
*	This function will be responsible for calling malloc the first time to allocate space 
*	for the two lists of myMalloc. After this initial call malloc will not be used again.
*/
int myInit(int numCores)
{
	int node; // loop invariant building list of nodes
	struct SMALL_BLOCK *small_list_ptr; // used to iterate over the small list and update the pointers
	struct LARGE_BLOCK *large_list_ptr; // used to iterate over the large list and update the pointers

	
	headSmallList = malloc( sizeof(LINKED_LIST_SMALL) );  // malloc 128k bytes plus room for meta data
	headLargeList = malloc( sizeof(LINKED_LIST_LARGE) );  // malloc 128k bytes plus room for meta data


	if ( headSmallList == NULL ||  headLargeList == NULL )
	{
		if (DEBUG)
		{
			printf("Failed to allocate memory for malloc init\n");
		}
		return -1;
	}

	// set up linked list for large chunks
	for ( node = 0; node < MAX_NUM_SMALL_BLOCKS; node++ )
	{
		small_list_ptr = &headSmallList->smallList[node];
		small_list_ptr->type = 0;
		
		if ( node != MAX_NUM_SMALL_BLOCKS )
		{
			small_list_ptr->next = &headSmallList->smallList[node + 1];
		}
		else
			small_list_ptr->next =  NULL; // the last element will point to NULL

	}



	// set up linked list for large chunks
	for ( node = 0; node < MAX_NUM_LARGE_BLOCKS; node++ )
	{
		large_list_ptr = &headLargeList->largeList[node];
		large_list_ptr->type = 1;
		
		if ( node != MAX_NUM_LARGE_BLOCKS )
		{
			large_list_ptr->next = &headLargeList->largeList[node + 1];
		}
		else
			large_list_ptr->next =  NULL; // the last element will point to NULL

	}

	// stack pointer init
	largeStack = &headLargeList->largeList[0]; // set the stack pointer at the first element in the large list
	smallStack = &headSmallList->smallList[0]; // set the stack pointer at the first element in the small list

	return 0;
}

/*
*	This function will malloc space upon user request. Two sizes will be given out, 64 bytes or 1k
*	chunks depening on the size that is requested.
*/
void *myMalloc(int size)
{
	
	char *returnMemoryAddress; // the address of memory return to the user
	

	pthread_mutex_lock(&L); // ensure only one thread in CS
	// if the size is between 1 and 64 inclusive hand out small chunk
	if ( 0 < size && size <= 64 ) 
	{
		// error checking
		if ( DEBUG && numSmallBlocks == MAX_NUM_SMALL_BLOCKS )
		{
			printf("User tried to get too many small blocks of mem\n");
		}

		returnMemoryAddress = smallStack->data;
		smallStack = smallStack->next; // pop of the head of list and move pointer to the next element
		numSmallBlocks++; // increment the number of small blocks allocated
	}
	else if( 64 < size && size <= 1024 ) // if the size is greater than 64 and less than or equal to 1k hand out a 1k chunk
	{
		if ( DEBUG && numLargeBlocks == MAX_NUM_LARGE_BLOCKS )
		{
			printf("User tried to get too many large blocks of mem\n");
		}

		returnMemoryAddress = largeStack->data;
		largeStack = largeStack->next; // pop of the head of list and move pointer to the next element
		numLargeBlocks++; // increment the number of small blocks allocated
	}
	else

	{
		if ( DEBUG )
		{
			printf("A size not identified for memory has been choosen\n");
			exit(1);
		}
	}

	pthread_mutex_unlock(&L); // unlock the CS
	return returnMemoryAddress;
}

/*
*	This function will free memory that has been allocated to the user. The free list is kept as a stack. When the node is freed the head will
*	point to this new freed node. The freed node will point to what was the head before it was inserted into the freelist. 
*/
void myFree(void *ptr)
{
	pthread_mutex_lock(&L);

	int typeOfBlock; // the type of memory block requesting to be freed
	struct LARGE_BLOCK *large_ptr; // used to append the new block to the front of the stack
	struct SMALL_BLOCK *small_ptr; // used to append the new block to the front of the stack

	large_ptr = (struct LARGE_BLOCK*)((char*)ptr - 16); 
	small_ptr = (struct SMALL_BLOCK*)((char*)ptr - 16);

	typeOfBlock = large_ptr->type; // find out what type of block it is

	if ( 1 == typeOfBlock )
	{
		
		large_ptr->next = largeStack; // freed node now points to previous first node
		largeStack = large_ptr; // head will now point to the new freed node
		numLargeBlocks--; // decrement the current number of large blocks allocated
	} 
	else if ( 0 == typeOfBlock )
	{
		small_ptr->next = smallStack; // freed node now points to previous first node
		smallStack = small_ptr; // head will now point to the new freed node
		numSmallBlocks--; // decrement the current number of small blocks allocated
	}
	else
	{
		if ( DEBUG )
		{
		printf("Problem with type when searching in memory\n");
		exit(1);
		}
	}

	pthread_mutex_unlock(&L);
}

