/*	
*	Programmed by Michael Humphrey
*	libmymalloc4 is a paralized version of mymalloc and free. The purpose of the program is
*	to simulate the malloc and free functions provided in the c library. The program intializes
*	256k bytes to allocate to the user upon request. Each thread will have it's own local list for allocation. In addition to
*	it's local list an overflow list will be used to for all threads. If the thread exhausts it's local list it will store to the overflow list.
*   The actual amount of memory reserved upon startup will be slightly larger since it will include metadata needed to maintain lists.
*	Two lists will be available for allocation from a small list, that will include blocks less than
*	or equal to 64 bytes and a large list that will handle anything larger than 64 bytes.
*	The larger list will hand out 1k for all requests and the small list will allocate
*	64 bytes. The lists will be maintained as a singly linked list that will be accessed
*	like a stack.
*/

#include "myMalloc.h"
#include "libmymalloc.h"

#define MAX_NUMBER_CORES 8

// GLOBALS
struct LINKED_LIST_SMALL *localSmallists[MAX_NUMBER_CORES]; // array of pointers, each one pointer to a different local list for small chunks
struct LINKED_LIST_LARGE *localLargeLists[MAX_NUMBER_CORES]; // array of pointers, each one pointer to a different local list for large chunks

struct LINKED_LIST_SMALL *sharedListSmall; // shared overflow list for small chunks
struct LINKED_LIST_LARGE *sharedListLarge; // shared overflow list for large chucks

struct LARGE_BLOCK *largeStack[MAX_NUMBER_CORES]; // used to point to the current free memory in the large list
struct SMALL_BLOCK *smallStack[MAX_NUMBER_CORES]; // used to point to the current free memory in the small list

struct LARGE_BLOCK *largeStackOverFlow; // used to point to the current free memory in the large list
struct SMALL_BLOCK *smallStackOverFlow; // used to point to the current free memory in the small list

int numSmallBlocks[MAX_NUMBER_CORES] = {0}; // keep count of the number of small blocks, since they will be maintained in one single stack
int numLargeBlocks[MAX_NUMBER_CORES] = {0}; // keep count of the number of large blocks, since they will be maintained in one single stack

pthread_key_t key; // key reference to find out which thread made call
int threadIDs[MAX_NUMBER_CORES] = {0, 1, 2, 3, 4, 5, 6, 7}; // used to assign id's during malloc
int currentThread = 0; //will be used to assign ID's to threads

pthread_mutex_t L; 

// END GLOBALS

/*
*	This function will be responsible for calling malloc the first time to allocate space 
*	for the two lists of myMalloc. After this initial call malloc will not be used again.
*/
int myInit(int numCores)
{
	int node, list; // loop invariant building list of nodes
	struct SMALL_BLOCK *small_list_ptr; // used to iterate over the small list and update the pointers
	struct LARGE_BLOCK *large_list_ptr; // used to iterate over the large list and update the pointers

	pthread_key_create(&key, NULL); // intialize the key for threadIDs
	

	// iterate through each local list and set up pointers
	for (list = 0; list < numCores; list++ ) 
	{

		localSmallists[list] = malloc( sizeof(LINKED_LIST_SMALL) );  // malloc 128k bytes plus room for meta data
		localLargeLists[list] = malloc( sizeof(LINKED_LIST_LARGE) );  // malloc 128k bytes plus room for meta data

		// check to make sure malloc returned correctly with memory
		if ( localSmallists[list] == NULL ||  localLargeLists[list] == NULL )
		{
			if (DEBUG)
			{
			printf("Failed to allocate memory for malloc init\n");
			}
			return -1;
		}

		// set up linked list for large chunks
		for ( node = 0; node < MAX_NUM_SMALL_BLOCKS; node++ )
		{
			small_list_ptr = &localSmallists[list]->smallList[node];
			small_list_ptr->type = 0;

			if ( node != MAX_NUM_SMALL_BLOCKS )
			{
				small_list_ptr->next = &localSmallists[list]->smallList[node + 1];
			}
			else
			small_list_ptr->next =  NULL; // the last element will point to NULL

		}


		
		// set up linked list for large chunks
		for ( node = 0; node < MAX_NUM_LARGE_BLOCKS; node++ )
		{
			large_list_ptr = &localLargeLists[list]->largeList[node];
			large_list_ptr->type = 1;
		
			if ( node != MAX_NUM_LARGE_BLOCKS )
			{
				large_list_ptr->next = &localLargeLists[list]->largeList[node + 1];
			}
			else
				large_list_ptr->next =  NULL; // the last element will point to NULL

		}

		// stack pointer init
		largeStack[list] = &localLargeLists[list]->largeList[0]; // set the stack pointer at the first element in the large list
		smallStack[list] = &localSmallists[list]->smallList[0]; // set the stack pointer at the first element in the small list

	}


	sharedListLarge = malloc ( sizeof(LINKED_LIST_LARGE) );
	sharedListSmall = malloc ( sizeof(LINKED_LIST_SMALL) );

	// check to make sure malloc returned correctly with memory
	if ( sharedListLarge == NULL ||  sharedListSmall == NULL )
	{
		if (DEBUG)
		{
			printf("Failed to allocate memory for malloc init\n");
		}
		return -1;
	}

	// set up over flow list for small chunks
	for ( node = 0; node < MAX_NUM_SMALL_BLOCKS; node++ )
	{
		small_list_ptr = &sharedListSmall->smallList[node];
		small_list_ptr->type = 2;
		
		if ( node != MAX_NUM_SMALL_BLOCKS )
		{
			small_list_ptr->next = &sharedListSmall->smallList[node + 1];
		}
		else
			small_list_ptr->next =  NULL; // the last element will point to NULL

	}


	// set up over flow list for large chunks
	for ( node = 0; node < MAX_NUM_LARGE_BLOCKS; node++ )
	{
		large_list_ptr = &sharedListLarge->largeList[node];
		large_list_ptr->type = 3;
		
		if ( node != MAX_NUM_LARGE_BLOCKS )
		{
			large_list_ptr->next = &sharedListLarge->largeList[node + 1];
		}
		else
			large_list_ptr->next =  NULL; // the last element will point to NULL

	}

	// stack pointer init for overflow
	largeStackOverFlow = &sharedListLarge->largeList[0]; // set the stack pointer at the first element in the large list
	smallStackOverFlow = &sharedListSmall->smallList[0]; // set the stack pointer at the first element in the small list

	return 0;
} /* end myInit */

/*
*	This function will malloc space upon user request. Two sizes will be given out, 64 bytes or 1k
*	chunks depening on the size that is requested.
*/
void *myMalloc(int size)
{
	char *returnMemoryAddress = NULL; // the address of memory return to the user
	int my_id; // the id of the calling thread

	if ( pthread_getspecific(key) == NULL ) {
		pthread_mutex_lock(&L);
		pthread_setspecific(key, (const void *)&threadIDs[currentThread]);
		currentThread++;
		pthread_mutex_unlock(&L);
	}

	my_id =	*((int *)(pthread_getspecific(key)));
	

	// if the size is between 1 and 64 inclusive hand out small chunk
	if ( 0 < size && size <= 64 ) 
	{
		// error checking
		if ( numSmallBlocks[my_id] == MAX_NUM_SMALL_BLOCKS )
		{
			pthread_mutex_lock(&L);
			returnMemoryAddress = smallStackOverFlow->data;
			smallStackOverFlow = smallStackOverFlow->next; // pop of the head of list and move pointer to the next element
			pthread_mutex_unlock(&L);
		}
		else 
		{
			returnMemoryAddress = (smallStack[my_id])->data;
			smallStack[my_id] = (smallStack[my_id])->next; // pop of the head of list and move pointer to the next element
			numSmallBlocks[my_id]++; // increment the number of small blocks allocated
		}
	}
	else if( 64 < size && size <= 1024 ) // if the size is greater than 64 and less than or equal to 1k hand out a 1k chunk
	{
		if ( numLargeBlocks[my_id] == MAX_NUM_LARGE_BLOCKS )
		{
			pthread_mutex_lock(&L);
			returnMemoryAddress = largeStackOverFlow->data;
			largeStackOverFlow = largeStackOverFlow->next; // pop of the head of list and move pointer to the next element
			pthread_mutex_unlock(&L);
		}
		else
		{
			returnMemoryAddress = (largeStack[my_id])->data;
			largeStack[my_id] = (largeStack[my_id])->next; // pop of the head of list and move pointer to the next element
			numLargeBlocks[my_id]++; // increment the number of small blocks allocated
		}
	}
	else

	{
		if ( DEBUG )
		{
			printf("A size not identified for memory has been choosen\n");
			exit(1);
		}
	}

	return returnMemoryAddress;
} /* end myMalloc */

/*
*	This function will free memory that has been allocated to the user. The free list is kept as a stack. When the node is freed the head will
*	point to this new freed node. The freed node will point to what was the head before it was inserted into the freelist. 
*/
void myFree(void *ptr)
{
	int typeOfBlock; // the type of memory block requesting to be freed
	struct LARGE_BLOCK *large_ptr; // used to append the new block to the front of the stack
	struct SMALL_BLOCK *small_ptr; // used to append the new block to the front of the stack

	int my_id = *((int *)(pthread_getspecific(key)));
	
	large_ptr = (struct LARGE_BLOCK*)((char*)ptr - 16); 
	small_ptr = (struct SMALL_BLOCK*)((char*)ptr - 16);

	typeOfBlock = large_ptr->type; // find out what type of block it is

	if ( 0 == typeOfBlock )
	{
		small_ptr->next = smallStack[my_id]; // freed node now points to previous first node
		smallStack[my_id] = small_ptr; // head will now point to the new freed node
		numSmallBlocks[my_id]--; // decrement the current number of small blocks allocated
	} 
	else if ( 1 == typeOfBlock )
	{

		large_ptr->next = largeStack[my_id]; // freed node now points to previous first node
		largeStack[my_id] = large_ptr; // head will now point to the new freed node
		numLargeBlocks[my_id]--; // decrement the current number of large blocks allocated
	}
	else if ( 2 == typeOfBlock )
	{
		pthread_mutex_lock(&L);
		small_ptr->next = smallStackOverFlow; // freed node now points to previous first node
		smallStackOverFlow = small_ptr; // head will now point to the new freed node
		pthread_mutex_unlock(&L);
	}
	else if ( 3 == typeOfBlock )
	{
		pthread_mutex_lock(&L);
		large_ptr->next = largeStackOverFlow; // freed node now points to previous first node
		largeStackOverFlow = large_ptr; // head will now point to the new freed node
		pthread_mutex_unlock(&L);
	}
	else
	{
		if ( DEBUG )
		{
		printf("Problem with type when searching in memory\n");
		exit(1);
		}
	}
} /* end myFree */