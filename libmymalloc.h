#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#include<sys/types.h>

#define MAX_NUM_LARGE_BLOCKS 128
#define MAX_NUM_SMALL_BLOCKS 2048
#define SMALL_OFFSET 80
#define LARGE_OFFSET 1040
#define DEBUG 1

typedef struct LARGE_BLOCK {
	int type; // 0 for small and 1 for large
	struct LARGE_BLOCK *next; 
	char data[1024];
}LARGE_BLOCK;

typedef struct SMALL_BLOCK {
	int type; // 0 for small and 1 for large
	struct  SMALL_BLOCK *next; 
	char data[64];
}SMALL_BLOCK;

struct LINKED_LIST_SMALL {
	struct SMALL_BLOCK smallList[MAX_NUM_SMALL_BLOCKS];
}LINKED_LIST_SMALL;

struct LINKED_LIST_LARGE {
	struct LARGE_BLOCK largeList[MAX_NUM_LARGE_BLOCKS];
}LINKED_LIST_LARGE;